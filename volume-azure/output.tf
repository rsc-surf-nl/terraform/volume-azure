output "id" {
  value = azurerm_managed_disk.volume.id
}

output "volume_id" {
  value = azurerm_managed_disk.volume.id
}

output "volume_name" {
  value = azurerm_managed_disk.volume.name
}
